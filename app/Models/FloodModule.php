<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloodModule extends Model
{
    use HasFactory;
    protected $fillable=[
        'device_id',
        'lat',
        'lng',
        'status',
    ];

    public function sensorData()
    {
        return $this->hasMany(FloodModuleDetails::class, 'flood_module_id');
    }
}
