<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FireModule extends Model
{
    use HasFactory;
    protected $fillable=[
        'device_id',
        'lat',
        'lng',
        'status',
    ];

    public function sensorData()
    {
        return $this->hasMany(FireModuleDetails::class, 'fire_module_id');
    }
}
