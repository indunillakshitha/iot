<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FireModuleDetails extends Model
{
    use HasFactory;
    protected $fillable =[
        'fire_module_id',
        'temp',
        'ir',
        'hum',
    ];

    /**
     * Get the user that owns the FireModuleDetails
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fireModule()
    {
        return $this->belongsTo(FireModule::class, 'fire_module_id');
    }
}
