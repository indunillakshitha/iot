<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloodModuleDetails extends Model
{
    use HasFactory;
    protected $fillable =[
        'flood_module_id',
        'temp',
        'water_level',
        'humidity',
        'wind_speed',
        'water_speed',
        'is_raining',
    ];

/**
 * Get the user that owns the FireModuleDetails
 *
 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 */
public function floodModule()
{
    return $this->belongsTo(FloodModule::class, 'flood_module_id');
}
}
