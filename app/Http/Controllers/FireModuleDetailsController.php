<?php

namespace App\Http\Controllers;

use App\Models\FireModule;
use App\Models\FireModuleDetails;
use Illuminate\Http\Request;

class FireModuleDetailsController extends Controller
{
    public function store(Request $request){

        $module=FireModule::where('device_id',$request->device_id)->first();
        $data =new FireModuleDetails();
        $data ->fire_module_id=$module->id;
        $data ->temp=$request->temp;
        $data ->ir=$request->ir;
        $data ->hum=$request->hum;
        $data->save();
        return;
    }
}
