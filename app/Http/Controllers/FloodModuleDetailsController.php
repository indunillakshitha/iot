<?php

namespace App\Http\Controllers;

use App\Models\FireModule;
use App\Models\FloodModule;
use App\Models\FloodModuleDetails;
use Illuminate\Http\Request;

class FloodModuleDetailsController extends Controller
{
    public function store(Request $request){

        $module=FloodModule::where('device_id',$request->device_id)->first();
        $data =new FloodModuleDetails();
        $data ->flood_module_id=$module->id;
        $data ->temp=$request->temp;
        $data ->water_level=$request->water_level;
        $data ->wind_speed=$request->wind_speed;
        $data ->water_speed=$request->water_speed;
        $data ->humidity=$request->humidity;
        $data->save();
        return;
    }
}
