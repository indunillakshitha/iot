<?php

namespace App\Http\Controllers;

use App\Models\FloodModule;
use App\Models\FloodModuleDetails;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FloodModuleController extends Controller
{
    public function dashboard()
    {
        
        $TempChartData=FloodModuleDetails::whereDate('created_at', Carbon::today())
        ->select('temp','created_at')
        ->groupBy('flood_module_id','temp','created_at')
        ->get();
        
        // $dta = [];
        // $dta['name']='asdsa'; 
        // $values =[];
        // $values =$TempChartData;
        // $dta->push($values);
        // return $dta;
        return Inertia::render('FloodModule/Dashboard');
    }
        public function index()
    {
        $moduleDetails = FloodModule::all();
        return Inertia::render('FloodModule/Index',compact('moduleDetails'));
    }
    public function initializeModule(Request $request)
    {
        
    }
    public function initializeModuleV($id)
    {
        
    }
    public function updateModule(Request $request)
    {
       

            $FloodModule = FloodModule::where('id',$request->id)->first();
            $FloodModule->status=1;
            $FloodModule->lat=$request->lat;
            $FloodModule->lng=$request->lng;
    
            $FloodModule->save();
            return redirect( )->route('flood.index');
        
        
    }
    public function updateModuleV($id)
    {
       
        $moduleDetail = FloodModule::where('id',$id)->first();
        return Inertia::render('FloodModule/Edit',compact('moduleDetail'));
        
    }
    public function viewAllData()
    {
        
    }
    public function filter(Request $request)
    {
        
    }
    public function view($id)
    {
         $moduleDetails =FloodModuleDetails::with('floodModule')
                        ->where('flood_module_id',$id)
                        ->orderBy('id','desc')
                        ->get();
        return Inertia::render('FloodModule/View',compact('moduleDetails'));
    }
        public function store()
    {
       
      try{

          $deviceId= 'FLOOD'.FloodModule::count();
          $FloodModule =new FloodModule();
          $FloodModule->status=0;
          $FloodModule->device_id=$deviceId;
  
          $FloodModule->save();
          return response()->json(['deviceId'=>$deviceId],200);
      }catch(Exception $e){

          return response()->json(['error'=>$e->getMessage()],500);
      }
    }
}
