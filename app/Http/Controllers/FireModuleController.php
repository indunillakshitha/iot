<?php

namespace App\Http\Controllers;

use App\Models\FarmModuleDetails;
use App\Models\FireModule;
use App\Models\FireModuleDetails;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Inertia\Inertia;
use PhpParser\Node\Expr\Cast\Object_;

class FireModuleController extends Controller
{
    public function dashboard()
    {
        
        $TempChartData=FireModuleDetails::whereDate('created_at', Carbon::today())
        ->select('temp','created_at')
        ->groupBy('fire_module_id','temp','created_at')
        ->get();
        
        // $dta = [];
        // $dta['name']='asdsa'; 
        // $values =[];
        // $values =$TempChartData;
        // $dta->push($values);
        // return $dta;
        return Inertia::render('FireModule/Dashboard');
    }
        public function index()
    {
        $moduleDetails = FireModule::all();
        return Inertia::render('FireModule/Index',compact('moduleDetails'));
    }
    public function initializeModule(Request $request)
    {
        
    }
    public function initializeModuleV($id)
    {
        
    }
    public function updateModule(Request $request)
    {
       

            $fireModule = FireModule::where('id',$request->id)->first();
            $fireModule->status=1;
            $fireModule->lat=$request->lat;
            $fireModule->lng=$request->lng;
    
            $fireModule->save();
            return redirect( )->route('fire.index');
        
        
    }
    public function updateModuleV($id)
    {
       
        $moduleDetail = FireModule::where('id',$id)->first();
        return Inertia::render('FireModule/Edit',compact('moduleDetail'));
        
    }
    public function viewAllData()
    {
        
    }
    public function filter(Request $request)
    {
        
    }
    public function view($id)
    {
         $moduleDetails =FireModuleDetails::with('fireModule')
                        ->where('fire_module_id',$id)
                        ->orderBy('id','desc')
                        ->get();
        return Inertia::render('FireModule/View',compact('moduleDetails'));
    }
        public function store()
    {
       
      try{

          $deviceId= 'FIRE'.FireModule::count();
          $fireModule =new FireModule();
          $fireModule->status=0;
          $fireModule->device_id=$deviceId;
  
          $fireModule->save();
          return response()->json(['deviceId'=>$deviceId],200);
      }catch(Exception $e){

          return response()->json(['error'=>$e->getMessage()],500);
      }
    }
}
