<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloodModuleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flood_module_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('flood_module_id');
            $table->decimal('temp',20,2)->default(0);
            $table->decimal('water_level',20,2)->default(0);
            $table->decimal('humidity',20,2)->default(0);
            $table->decimal('wind_speed',20,2)->default(0);
            $table->decimal('water_speed',20,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flood_module_details');
    }
}
