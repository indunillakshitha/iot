<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFireModuleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fire_module_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fire_module_id');
            $table->decimal('temp',20,2)->default(0);
            $table->boolean('ir')->default(0);
            $table->decimal('hum',20,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fire_module_details');
    }
}
