<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmModuleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farm_module_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('farm_module_id');
            $table->decimal('ph',20,2)->default(0);
            $table->decimal('soil_moistuerel',20,2)->default(0);
            $table->decimal('temperature',20,2)->default(0);
            $table->decimal('wind_speed',20,2)->default(0);
            $table->decimal('water_speed',20,2)->default(0);
            $table->decimal('water_tank_level',20,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farm_module_details');
    }
}
