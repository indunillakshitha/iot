<?php

use App\Http\Controllers\BorrowerController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CenterController;
use App\Http\Controllers\FireModuleController;
use App\Http\Controllers\FloodModuleController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\LoanCategoryController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\RepaymentController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    // return Inertia::render('Dashboard');
    return Inertia::render('AdminDashboard');
})->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/profile-dashboard', function () {
    return Inertia::render('Dashboard');
})->name('profile.dashboard');
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {

    Route::prefix('/users')->group(function () { 
        //Users section
        Route::get('/index', [UserController::class, 'index'])->name('users.index');
        Route::get('/create', [UserController::class, 'create'])->name('users.create');
        Route::post('/store', [UserController::class, 'store'])->name('users.store');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/update', [UserController::class, 'update'])->name('users.update');
       
    });

    Route::prefix('roles')->group(function () { 
        //Roles section
        Route::get('/index', [RoleController::class, 'index'])->name('roles.index');
        Route::get('/create', [RoleController::class, 'create'])->name('roles.create');
        Route::post('/store', [RoleController::class, 'store'])->name('roles.store');
        Route::get('/show/{id}', [RoleController::class, 'show'])->name('roles.show');
        Route::post('/update', [RoleController::class, 'update'])->name('roles.update');
        Route::get('/destroy/{id}', [RoleController::class, 'destroy'])->name('roles.destroy');
       
    });
    Route::prefix('fire-module')->group(function () { 
        //Roles section
        Route::get('/index', [FireModuleController::class, 'index'])->name('fire.index');
        Route::get('/viewAllData', [FireModuleController::class, 'viewAllData'])->name('fire.viewAllData');
        Route::post('/filter', [FireModuleController::class, 'filter'])->name('fire.filter');
        Route::get('/dashboard', [FireModuleController::class, 'dashboard'])->name('fire.dashboard');
        Route::get('/initializeModule/{id}', [FireModuleController::class, 'initializeModuleV'])->name('fire.initializeModuleV');
        Route::post('/initializeModule', [FireModuleController::class, 'initializeModule'])->name('fire.initializeModule');
        Route::get('/view/{id}', [FireModuleController::class, 'view'])->name('fire.view');
        Route::get('/updateModule/{id}', [FireModuleController::class, 'updateModuleV'])->name('fire.updateModuleV');
        Route::post('updateModule', [FireModuleController::class, 'updateModule'])->name('fire.updateModule');
       
    });
    Route::prefix('flood-module')->group(function () { 
        //Roles section
        Route::get('/index', [FloodModuleController::class, 'index'])->name('flood.index');
        Route::get('/viewAllData', [FloodModuleController::class, 'viewAllData'])->name('flood.viewAllData');
        Route::post('/filter', [FloodModuleController::class, 'filter'])->name('flood.filter');
        Route::get('/dashboard', [FloodModuleController::class, 'dashboard'])->name('flood.dashboard');
        // Route::get('/initializeModule', [FloodModuleController::class, 'initializeModuleV'])->name('flood.initializeModuleV');
        // Route::post('/initializeModule', [FloodModuleController::class, 'initializeModule'])->name('flood.initializeModule');
        Route::get('/view/{id}', [FloodModuleController::class, 'view'])->name('flood.view');        
        Route::get('/updateModule/{id}', [FloodModuleController::class, 'updateModuleV'])->name('flood.updateModuleV');
        Route::post('updateModule', [FloodModuleController::class, 'updateModule'])->name('flood.updateModule');
       
    });



    
});