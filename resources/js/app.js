require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import ChartKick from 'vue-chartkick';
import Chart from 'chart.js';
const el = document.getElementById('app');

createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
        methods: {
            show_swal(text, icon){
                return this.$swal({
                    toast: true,
                    // title ,
                    text ,
                    icon ,
                    position: 'top-end',
                    timer: 3000,
                    timerProgressBar: true,
                    width: '200px',
                    height: '100px',
                })
                ;
            },
        }
})
    .mixin({ methods: { route } })
    .use(InertiaPlugin)
    .use(VueSweetalert2)
    .use(ChartKick)
    .use(ChartKick.use(Chart))
    .mount(el);

InertiaProgress.init({ color: '#4B5563' });
