const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
            // 'vue$': 'vue/dist/vue.common.js',
            // 'jquery': 'jquery/src/jquery.js'
        },
    },
};
